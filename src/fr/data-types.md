# Types de données

La référence du langage liste pas moins de 23 types de données (et ce n'est même pas exhaustif), du plus simple au plus complexe.

Chacun de ces types apporte son lot de représentations et de fonctionnalités.

Certains de ces types sont assez communs dans les langages de programmation (booléens, nombres, chaines de caractères). D'autres sont plus exotiques (symboles).

Dans les sous-chapitres suivants, je mettrai en oeuvre rapidement et simplement ces types de données en guise d'illustration (je ne couvrirai pas toutes leurs fonctionnalités).
