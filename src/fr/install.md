# Installer Guile

## Installation

### Pour GNU/Linux

Installer Guile est simple comme bonjour via un gestionnaire de paquet.

**Avec Guix (recommandé)**

```bash
$ guix install guile
```

**Avec apt (Debian et dérivés)**

systèmes fondés sur debian stable

```bash
$ apt install guile-2.0
```

systèmes fondés sur debian testing ou unstable

```bash
$ apt install guile-3.0
```

**Avec Yum (RedHat et dérivés)**

```bash
$ yum install guile
```

**Avec Pacman (ArchLinux et dérivés)**

```bash
$ pacman -S guile
```

### Pour MacOS

Une procédure pour MacOS est disponible [ici](https://github.com/aconchillo/homebrew-guile).

### Pour Windows

Tu peux installer un système Ubuntu (depuis le store), ensuite, tu peux suivre les instructions pour le gestionnaire de paquet apt pour installer Guile et commencer à hacker !

## Configuration

Guile est équipé d’un outil que l’on nomme communemment un REPL (Read-Eval-Print-Loop), une sorte de super interpréteur. Mais sans configuration, il reste très rudimentaire et s’en retrouve peu pratique.

![repldown](images/guile-brut.gif)

Rien de bien compliqué, mais il faut activer deux modules par défaut (dont un que tu dois installer manuellement).

```bash
$ guix package -i guile-colorized
```

Si tu n’utilises pas Guix, tu trouveras les instructions d’installation de ce module [ici](https://gitlab.com/NalaGinrut/guile-colorized).

Ensuite, tu peux configurer ton interpréteur en modifiant le fichier `~/.guile` (crée-le s’il n’existe pas) et mets y les lignes suivantes :

```scheme
(use-modules (ice-9 readline)
             (ice-9 colorized))

(activate-readline)
(activate-colorized)
```

Avec ça, ton REPL est maintenant plus coloré, dispose d’un historique des évaluations passées navigable et une aide visuelle à la fermeture d’une parenthèse.

![replup](images/guile-net.gif)

## Outillage

### Editeur

L’éditeur est un choix très personnel. Cependant, [Emacs](https://www.gnu.org/software/emacs/) reste l’éditeur qui fourni le meilleur support pour Guile.

Tu peux l’installer de la même manière que tu as installé Guile (ci-dessous, exemple avec Guix) :

```bash
$ guix install emacs
```
