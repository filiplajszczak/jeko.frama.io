[Home](./home.md)

- [Install Guile](./install.md)
- [Hello World](./hello.md)
- [Data types](./data-types.md)
    - [Booleans](./booleans.md)
    - [Numbers](./numbers.md)
	- [Characters](./char.md)
